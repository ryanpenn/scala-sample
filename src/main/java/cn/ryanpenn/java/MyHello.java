package cn.ryanpenn.java;

/**
 * MyHello
 */
public class MyHello {
    public String sayHello(String name) {
        return "Hello " + name;
    }
}
