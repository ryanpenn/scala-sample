package cn.ryanpenn.scala

import cn.ryanpenn.java.MyHello

/**
  * Hello world!
  *
  */
object App {
  def main(args: Array[String]): Unit = {
    val hello = new MyHello()
    println(hello.sayHello("Scala World!"))
  }
}
